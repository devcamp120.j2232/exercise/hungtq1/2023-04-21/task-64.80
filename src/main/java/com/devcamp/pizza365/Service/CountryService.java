package com.devcamp.pizza365.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.repository.CountryRepository;

@Service
public class CountryService {
  @Autowired
  private CountryRepository countryRepository;

  public CCountry createCountry(CCountry cCountry) {
    try {
      // CCountry newCountry = new CCountry();
      // newCountry.setCountryName(cCountry.getCountryName());
      // newCountry.setCountryCode(cCountry.getCountryCode());
      // newCountry.setRegions(cCountry.getRegions());
      // CCountry savedRole = countryRepository.save(newCountry);
      CCountry savedRole = countryRepository.save(cCountry);
      return savedRole;
    } catch (Exception e) {
      return null;
    }
  }
}
